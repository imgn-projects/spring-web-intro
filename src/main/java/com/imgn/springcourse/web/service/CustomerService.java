package com.imgn.springcourse.web.service;

import com.imgn.springcourse.web.domain.Customer;
import com.imgn.springcourse.web.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService {

    private CustomerRepository customerRepository;

    @Autowired
    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public List<Customer> listAllCustomers() {
        return customerRepository.findAll();
    }

    public List<Customer> listAllByLastName(String lastName) {
        return customerRepository.findByLastName(lastName);
    }
}
